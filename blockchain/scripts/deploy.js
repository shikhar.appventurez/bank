const fs = require('fs');
const { ethers } = require('hardhat');

async function main(){
    const BankContract = await ethers.getContractFactory("Bank");
    const bank = await BankContract.deploy();
    await bank.deployed();
    console.log("The bank contract was deployed to: " + bank.address );

    const TokenContract = await ethers.getContractFactory("Token");
    const token = await TokenContract.deploy(bank.address);
    await token.deployed();
    console.log("The token contract was deployed to: " + token.address);

    let addresses = {"bankContract" : bank.address, "tokenContract": token.address};
    let addressesJSON = JSON.stringify(addresses);
    fs.writeFileSync("environment/contract-address.json", addressesJSON)

}

main()
.then(()=>{
    process.exit(0);
})
.catch((error)=>{
    console.log(error);
    process.exit(1);
})