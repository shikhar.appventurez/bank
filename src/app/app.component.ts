declare let window: any;
import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup } from '@angular/forms';
import { ethers } from 'ethers';

import addresses from '../../environment/contract-address.json'
import Bank from '../../blockchain/artifacts/blockchain/contracts/Bank.sol/Bank.json'
import Token from '../../blockchain/artifacts/blockchain/contracts/Token.sol/Token.json'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title(title: any) {
    throw new Error('Method not implemented.');
  }
  public depositForm: FormGroup;
  public withdrawForm: FormGroup;

  public signer: any;

  public bankContract:any;
  public tokenContract: any;

  public userTotalToken: any;
  public userTotalAssests: any;
  public totalAssests: any;
  public signerAddress: any;

  constructor(){
    this.depositForm = new FormGroup({
      DepositAmount: new FormControl()
    });
    this.withdrawForm = new FormGroup({
      WithdrawAmount: new FormControl()
    });
  }

  async ngOnInit() {
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    await provider.send('eth_requestAccounts', []);

    provider.on("network", (newNetwork:any, oldNetwork: any) => {
      if(oldNetwork){
        window.location.reload();
      }
    });

    this.signer = provider.getSigner();

    if(await this.signer.getChainId() !== 5){
      alert("Please Change your network to goerli testnet!")
    }

    this.bankContract = new ethers.Contract(addresses.bankContract, Bank.abi, this.signer);
    this.tokenContract = new ethers.Contract(addresses.tokenContract, Token.abi, this.signer);

    this.userTotalAssests = ethers.utils.formatEther(await this.bankContract.accounts(await this.signer.getAddress()));
    this.totalAssests = ethers.utils.formatEther(await this.bankContract.totalAssets());
    this.userTotalToken = ethers.utils.formatEther(await this.tokenContract.balanceOf(await this.signer.getAddress()));
    this.signerAddress = await this.signer.getAddress();
console.log(ethers.utils.formatEther(await provider.getBalance(this.signerAddress)))
  }

  async deposit(){
    const tx = await this.bankContract.deposit(
      {value:ethers.utils.parseEther(this.depositForm.value.DepositAmount.toString())});
      await tx.wait();

      this.depositForm.reset();
      window.location.reload();
  }
  

  async withdraw(){
    const tx = await this.bankContract.withdraw(
      ethers.utils.parseEther(this.withdrawForm.value.WithdrawAmount.toString()),
      addresses.tokenContract
    );
    await tx.wait();

    this.withdrawForm.reset();
    window.location.reload();
  }
}
